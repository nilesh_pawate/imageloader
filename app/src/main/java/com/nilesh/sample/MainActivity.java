package com.nilesh.sample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        List<ImageViewModel> viewModels = new ArrayList<>();

        ImageViewModel im1 = new ImageViewModel("Content marketing", new ArrayList() {{
            add("http://www.intuitm.com/img/cm.jpg");
            add("http://raisonbrands.com/images/uploads/5.7_banner_contentmarketing.jpg");
            add("http://www.intuitm.com/img/cm.jpg");
        }});

        viewModels.add(im1);

        ImageViewModel im2 = new ImageViewModel("Content marketing", new ArrayList() {{
            add("http://www.intuitm.com/img/cm.jpg");
        }});

        viewModels.add(im2);

        ImageViewModel im3 = new ImageViewModel("Content marketing", new ArrayList() {{
            add("http://www.intuitm.com/img/cm.jpg");
            add("http://raisonbrands.com/images/uploads/5.7_banner_contentmarketing.jpg");
        }});

        viewModels.add(im3);

        ImageViewModel im4 = new ImageViewModel("Content marketing", new ArrayList() {{
            add("http://www.intuitm.com/img/cm.jpg");
            add("http://raisonbrands.com/images/uploads/5.7_banner_contentmarketing.jpg");
            add("http://www.intuitm.com/img/cm.jpg");
        }});

        viewModels.add(im4);

        ImageViewModel im5 = new ImageViewModel("Content marketing", new ArrayList() {{
            add("http://www.intuitm.com/img/cm.jpg");
        }});

        viewModels.add(im5);

        ImageViewModel im6 = new ImageViewModel("Content marketing", new ArrayList() {{
            add("http://www.intuitm.com/img/cm.jpg");
        }});

        viewModels.add(im6);

        ImageViewModel im7 = new ImageViewModel("Content marketing", new ArrayList() {{
            add("http://www.intuitm.com/img/cm.jpg");
            add("http://raisonbrands.com/images/uploads/5.7_banner_contentmarketing.jpg");
            add("http://www.intuitm.com/img/cm.jpg");
        }});

        viewModels.add(im7);
        ImageListAdapter adapter = new ImageListAdapter(this, viewModels);

        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
