package com.nilesh.sample;

import java.util.List;

/**
 * Created by nilesh.mp on 5/27/2016.
 */
public class ImageViewModel {

    private String title;
    private List<String> images;

    public ImageViewModel(String title, List<String> images) {
        this.title = title;
        this.images = images;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getImages() {
        return images;
    }
}
