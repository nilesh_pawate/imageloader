package com.nilesh.sample;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;

/**
 * Created by avinash.renukaradhya on 6/2/2016.
 */
public class GlideHelper {

    public static void loadImage(Context context, ImageViewModel imageViewModel,
                                 ImageView oneImageView, LinearLayout linearLayout,
                                 ImageView twoImageView, ImageView threeImageView){

     /*   final boolean[] topLoaded = {false, false};
        final boolean[] bottomLoaded = {false, false};
        final boolean[] mainUrlLoaded = {false, false};*/

        oneImageView.setVisibility(View.VISIBLE);
        twoImageView.setVisibility(View.VISIBLE);
        threeImageView.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);

        if (imageViewModel.getImages().size() == 3){

            Glide.with(context).load(imageViewModel.getImages().get(0))
                    .signature(new StringSignature(imageViewModel.getImages().get(0)))
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(oneImageView);

            Glide.with(context).load(imageViewModel.getImages().get(1))
                    .signature(new StringSignature(imageViewModel.getImages().get(1)))
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(twoImageView);

            Glide.with(context).load(imageViewModel.getImages().get(2))
                    .signature(new StringSignature(imageViewModel.getImages().get(2)))
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(threeImageView);


        } else if (imageViewModel.getImages().size() == 2) {

            threeImageView.setVisibility(View.GONE);

            Glide.with(context).load(imageViewModel.getImages().get(0))
                    .signature(new StringSignature(imageViewModel.getImages().get(0)))
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(oneImageView);

            Glide.with(context).load(imageViewModel.getImages().get(1))
                    .signature(new StringSignature(imageViewModel.getImages().get(1)))
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(twoImageView);

        } else if (imageViewModel.getImages().size() == 1){

            linearLayout.setVisibility(View.GONE);

            Glide.with(context).load(imageViewModel.getImages().get(0))
                    .signature(new StringSignature(imageViewModel.getImages().get(0)))
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(oneImageView);

        }




    }

}
