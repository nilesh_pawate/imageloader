package com.nilesh.sample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by nilesh.mp on 5/27/2016.
 */
public class ImageViewHolder extends RecyclerView.ViewHolder {

    ImageView image_one, image_two, image_three;
    LinearLayout linearLayout;
    Context context;

    public ImageViewHolder(Context context, View itemView) {
        super(itemView);
        image_one = (ImageView) itemView.findViewById(R.id.image_one);
        image_two = (ImageView) itemView.findViewById(R.id.image_two);
        image_three = (ImageView) itemView.findViewById(R.id.image_three);
        linearLayout = (LinearLayout) itemView.findViewById(R.id.image_holder);
        this.context = context;
    }

    public void onBind(ImageViewModel item, ImageListAdapter.onImageClicks listener) {

        GlideHelper.loadImage(context,item,image_one,linearLayout,image_two,image_three);

/*        List<String> images = item.getImages();

        image_one.setVisibility(View.VISIBLE);
        image_two.setVisibility(View.VISIBLE);
        image_three.setVisibility(View.VISIBLE);

//        if (images.size() == 2) {
//            image_three.setVisibility(View.GONE);
//        } else if (images.size() == 1) {
//            image_three.setVisibility(View.GONE);
//            image_two.setVisibility(View.GONE);
//        }

//        for (int i = 0; i < images.size(); i++) {
        String s = images.get(0);
        ImageLoader.with(image_one.getContext()).into(image_one).load(s).listener(new ImageLoader.ImageListener() {
            @Override
            public void onLoaded() {
//                image_one.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancel() {
                image_one.setVisibility(View.GONE);
            }
        }).build();
        String s1 = "";
        if (images.size() > 1) {
            s1 = images.get(1);
        }
        ImageLoader.with(image_two.getContext()).into(image_two).load(s1).listener(new ImageLoader.ImageListener() {
            @Override
            public void onLoaded() {
//                image_two.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancel() {
                image_two.setVisibility(View.GONE);
            }
        }).build();


        String s2 = "";
        if (images.size() > 2) {
            s2 = images.get(2);
        }
        ImageLoader.with(image_three.getContext()).load(s2).into(image_three).listener(new ImageLoader.ImageListener() {
            @Override
            public void onLoaded() {
//                image_three.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancel() {
                image_three.setVisibility(View.GONE);

            }
        }).build();
//                Glide.with(context)
//                        .load(images.get(i))
//                        .into(image_one);
//        }*/
    }
}
