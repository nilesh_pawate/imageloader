package com.nilesh.sample;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.nilesh.sample.transformations.BlurTransformation;
import com.nilesh.sample.transformations.RoundTransformation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public class ImageLoader {
    public static final float HALF_DENSITY = 0.5f;
    public static final float QUATER_DENSITY = 0.25f;
    private ImageView imageView;
    private String url;
    private int resourceId;
    private final Activity activity;
    private final Context context;
    private final Fragment fragment;

    //OPTIONAL
    private int placeHolderId;
    private final List<Integer> transformations = new ArrayList<>();
    private int animationId;
    private int errorPlaceHolderId;
    private ImageListener listener;
    private TargetListener target;
    private float sizeMultiplier = 1;
    private int width;
    private int height;
    private String key;

    public static class Transformations {

        public static final int ROUND = 0;
        public static final int BLUR = 1;
        public static final int CENTER_CROP = 2;
        public static final int FIR_CENTER = 3;

        @IntDef({ROUND, BLUR, FIR_CENTER, CENTER_CROP})
        @Retention(RetentionPolicy.SOURCE)
        public @interface Type {
        }

        @Type
        int transformation;

        public Transformations(@Type int transformation) {
            this.transformation = transformation;
        }

        @Type
        public int getType() {
            return transformation;
        }
    }

    public static ImageLoader with(Activity activity) {
        return new ImageLoader(activity);
    }

    public static ImageLoader with(Context context) {
        return new ImageLoader(context);
    }

    public static ImageLoader with(Fragment fragment) {
        return new ImageLoader(fragment);
    }

    private ImageLoader(Activity context) {
        this.activity = context;
        this.context = null;
        this.fragment = null;
    }

    private ImageLoader(Context context) {
        this.context = context;
        this.fragment = null;
        this.activity = null;
    }

    private ImageLoader(Fragment context) {
        this.fragment = context;
        this.context = null;
        this.activity = null;
    }

    /**
     * Each added transformation will be executed in order of which it was added
     *
     * @param flag Tranformation flag
     * @return instance of builder
     */
    public ImageLoader transform(@Transformations.Type int flag) {
        transformations.add(flag);
        return this;
    }

    public ImageLoader load(String url) {
        this.url = url;
        return this;
    }

    public ImageLoader load(int resourceId) {
        this.resourceId = resourceId;
        return this;
    }

    public ImageLoader listener(ImageListener listener) {
        this.listener = listener;
        return this;
    }

    public ImageLoader into(ImageView view) {
        this.imageView = view;
        return this;
    }

    public ImageLoader into(TargetListener target) {
        this.target = target;
        return this;
    }

    public ImageLoader size(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public ImageLoader key(String key) {
        this.key = key;
        return this;
    }

    public ImageLoader placeHolder(int resoruceId) {
        this.placeHolderId = resoruceId;
        return this;
    }

    /**
     * Resizes loaded images for give sizeMultiplayer. Doesnt work if used with Transformation due to glide bug
     *
     * @param sizeMultiplier
     * @return
     */
    public ImageLoader sizeMultiplier(float sizeMultiplier) {
        this.sizeMultiplier = sizeMultiplier;
        return this;
    }

    public ImageLoader error(int errorPlaceHolderId) {
        this.errorPlaceHolderId = errorPlaceHolderId;
        return this;
    }

    public ImageLoader animate(int animationId) {
        this.animationId = animationId;
        return this;
    }

    public Request build() {
        // Preconditions
        if (context == null && activity == null && fragment == null) {
            throw new IllegalArgumentException("either context or activity or fragment should be set");
        }

//        if (placeHolderId <= 0 && TextUtils.isEmpty(url) && resourceId <= 0) {
//            throw new IllegalArgumentException("nothing to load");
//        }

        if (imageView == null && target == null) {
            throw new IllegalArgumentException("imageView or targer missing");
        }

        if (sizeMultiplier < 0f || sizeMultiplier > 1f) {
            throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
        }

        // building
        Context localContext;
        RequestManager item;
        if (context != null) {
            localContext = context;
            item = Glide.with(context);
        } else if (activity != null) {
            localContext = activity;
            item = Glide.with(activity);
        } else {
            localContext = fragment.getActivity();
            item = Glide.with(fragment);
        }

        if (!isValidContext(localContext)) {
            return null;
        }

        DrawableRequestBuilder request = item.load(url != null ? url : resourceId);
        if (width != 0 && height != 0) {
            request = request.override(width, height);
        }
        if (animationId > 0) {
            request = request.animate(animationId);
        }
        if (placeHolderId > 0) {
            request = request.placeholder(placeHolderId);
        }
        if (errorPlaceHolderId > 0) {
            request = request.error(errorPlaceHolderId);
        }
        if (key == null) {
            key = url != null ? url : String.valueOf(resourceId);
        }
        imageView.setImageDrawable(null);

        if (listener != null) {
            request = request.listener(new RequestListener() {
                @Override
                public boolean onException(Exception e, Object model, Target target, boolean isFirstResource) {
                    listener.onCancel();
                    return false;
                }

                @Override
                public boolean onResourceReady(Object resource, Object model, Target target, boolean isFromMemoryCache, boolean isFirstResource) {
                    listener.onLoaded();
                    return false;
                }
            });
        }
        for (Integer trans : transformations) {
            switch (trans.intValue()) {
                case Transformations.ROUND:
                    request = request.transform(new RoundTransformation(localContext));
                    break;
                case Transformations.BLUR:
                    request = request.transform(new BlurTransformation(localContext));
                    break;
                case Transformations.CENTER_CROP:
                    request = request.centerCrop();
                    break;
                case Transformations.FIR_CENTER:
                    request = request.fitCenter();
                    break;
            }
        }
        if (request == null) {
            return null;
        }

        request.dontAnimate();

        if (imageView != null) {
            Target targetRequest = request.into(imageView);
            return new Request(targetRequest.getRequest(), key);
        }
        if (target != null) {
            SimpleTarget<Drawable> simpleTarget = new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(Drawable resource, GlideAnimation glideAnimation) {
                    target.onResourceReady(resource);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    target.onLoadFailed(e);
                }
            };
            request.into(simpleTarget);
            return new Request(simpleTarget.getRequest(), key);
        }
        return null;
    }

    private boolean isValidContext(@NonNull Context localContext) {
        if (localContext instanceof Activity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                return !((Activity) localContext).isFinishing() && !((Activity) localContext).isDestroyed();
            } else {
                return !((Activity) localContext).isFinishing();
            }
        }
        return true;
    }

    public static class Request {

        private final com.bumptech.glide.request.Request request;
        private final String key;

        public Request(com.bumptech.glide.request.Request request, String key) {
            this.request = request;
            this.key = key;
        }

        public void cancel() {
            request.clear();
            request.recycle();
        }

        public String getKey() {
            return key;
        }
    }

    public interface ImageListener {
        void onLoaded();

        void onCancel();
    }

    public abstract static class TargetListener {

        public void onResourceReady(Drawable bitmap) {
        }

        public void onLoadFailed(Exception exception) {
        }
    }

    public static void clear(View view) {
        Glide.clear(view);
    }
}
