package com.nilesh.sample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by nilesh.mp on 5/27/2016.
 */
public class ImageListAdapter extends RecyclerView.Adapter<ImageViewHolder> {
    private Context context;
    List<ImageViewModel> viewModels;
    public LayoutInflater inflater;


    ImageListAdapter(Context context, List<ImageViewModel> viewModels) {
        this.context = context;
        this.viewModels = viewModels;
        this.inflater = LayoutInflater.from(context);

    }

    onImageClicks listener;

    public void setListener(onImageClicks listener) {
        this.listener = listener;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.list_row_old, parent, false);
        ImageViewHolder viewHolder = new ImageViewHolder(context, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        holder.onBind(getItem(position), listener);
    }

    public ImageViewModel getItem(int position) {
        return viewModels.get(position);
    }

    @Override
    public int getItemCount() {
        return viewModels.size();
    }

    public interface onImageClicks {

    }
}
